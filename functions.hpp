#ifndef FUNCTIONS_CPP
#define FUNCTIONS_CPP

#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <dirent.h>
#include <algorithm>

#include "quick_tinyxml/tinyxml.h"

using namespace std;

#define SSTR( x ) static_cast< ostringstream & >( \
        ( ostringstream() << dec << x ) ).str()

class CSVRow{
    public:
        string const& operator[](size_t index) const{
            return m_data[index];
        }

        size_t size() const{
            return m_data.size();
        }

        void readNextRow(istream& str){
            string         line;
            getline(str, line);

            stringstream   lineStream(line);
            string         cell;

            m_data.clear();
            while(getline(lineStream, cell, ',')){
                cell.erase(remove(cell.begin(), cell.end(), ' '), cell.end());
                 cell.erase(remove(cell.begin(), cell.end(), '\n'), cell.end());
                 cell.erase(remove(cell.begin(), cell.end(), '\r'), cell.end());
                m_data.push_back(cell);
            
            }

            // This checks for a trailing comma with no data after it.
            if (!lineStream && cell.empty()){
                // If there was a trailing comma then add an empty element.
                m_data.push_back("");
            }
        }

    private:
        vector<string>    m_data;
};

struct Head{
    int frame;
    int id;
    int rotation;
	int h;
	int w;
	int x;
	int y;
};

//***SAV added
bool is_file(const char* path);

istream& operator>>(istream& str, CSVRow& data);

vector <string> arrayFilesName(string folderPath);

bool compareByframe(const Head &head1, const Head &head2);

// *** SAV it returns -1 if it fails so that the main program can carry on
// int readXMLviper2DamGT(string in_path, vector<Head>& heads);

void writeXMLviper2DamGT(string out_path, vector<Head>& heads);

void viper2dam(string in_path, string out_path);

void viper2csv(string in_path, string out_path, int expand=0, int width=325, int height=288);  // converts from Viper to CSV

void  viper2yolo (const string basename, const string in_file, const string out_path, int expand=0, int width=325, int height=288);

void mat2damDet(string in_path, string out_path);

void mat2damTrack(string in_path, string out_path);


#endif
