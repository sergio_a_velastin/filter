cmake_minimum_required (VERSION 2.8)
project( Filters )

add_executable( filter filter.cpp functions.cpp quick_tinyxml/tinyxml.cpp quick_tinyxml/tinyxmlerror.cpp quick_tinyxml/tinyxmlparser.cpp quick_tinyxml/tinystr.cpp )
