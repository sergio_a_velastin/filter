// M. González 2017, based on S.A. Velastin 2016
// Modified in Oct 2018 by SAV (S.A. Velastin)

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
#include <cstddef>
#include <sys/stat.h> 
#include <typeinfo>
#include <stdlib.h>

#include "functions.hpp"

using namespace std;

#define XML_SUFFIX "-Filt.xml"
#define CSV_SUFFIX "-Filt.csv"
// ***SAV "modes" just stores 0,1,2,3.... to define the valid modes (a bit of an overkill?)
#define MAX_MODES 5
#define DEF_EXPAND 0
#define DEF_WIDTH 352
#define DEF_HEIGHT 288

void displayUsage(){
    cout << "./filter -s path -d path [opts]\n";
    cout << "-s path: full path (no trailing /) where GT file(s) are\n";  
    cout << "-d path: path (no trailing /) where to store the output file(s)\n";
    cout << "-m mode: mode to apply the filter:\n";
    cout << "\t 1: ViPER to Damien (GT)" << endl;
    cout << "\t 2: Mathieu to Damien (detection)" << endl;
    cout << "\t 3: Mathieu to Damien (tracking)" << endl;
    cout << "\t 4: ViPER to CSV (default)" << endl; // ***SAV 2018
    cout << "\t 5: VIPER to Yolo" << endl; 
    cout << "-e fact: expand/compress by fact percentage (default: " << DEF_EXPAND << ")\n";
    cout << "-w width: width of the image (default: " << DEF_WIDTH <<")\n";   // these are used to clip values
    cout << "-h height: height of the image (default: "<< DEF_HEIGHT << ")\n";
}

int main(int argc, char** argv){
    char ch;
    string in_path, out_path, file_name;
    vector<int> modes;
    int n_modes = MAX_MODES, mode=4; // defaults CSV
    int expand=DEF_EXPAND;   // percentage of expansion of bbox (compression if negative)
    int width=DEF_WIDTH;  // we use defaults as for the PAMELA_ANDES dataset. these are CIF dimensions
    int height=DEF_HEIGHT;

    for(int i = 1; i <= n_modes; i++){
        modes.push_back(i);
    }
    cout << "Available modes: ";
    for(int i = 0; i < (int)modes.size(); i++){
        cout << modes[i] << " ";
    }
    cout << endl;
    
    if(argc < 2){
        cerr << "Missing arguments" << endl;
        displayUsage();
        return -1;
    }

    // Deal with the command line, see http://linux.die.net/man/3/optarg for handling commands
    // ***SAV need to find out format of this string as using :: results in segmentation error!
    while((ch = getopt(argc, argv, "s:d:m:e:w:h:")) != -1){
        switch(ch){
            case 's':
                in_path = optarg;
                break;
            case 'd':
                out_path = optarg;
                break;
            case 'm':
                mode = atoi(optarg);
                if(find(modes.begin(), modes.end(), mode) == modes.end()){
                    cerr << "Invalid mode: '" << mode << "' doesn't exist." << endl;
                    displayUsage();
                    exit(0);
                }
                break;
            case 'e':   // expansion
                expand=atoi(optarg);
                break;
            case 'w':
                width= atoi(optarg);
                break;
            case 'h':
                height= atoi(optarg);
                break;
            case '?':
                cerr << "Invalid option:  '" << char(ch) << "' doesn't exist." << endl << endl;
                displayUsage();
                exit(0);
            default:
                cerr << "Missing value for argument: '" << char(ch) << "'" << endl << endl;
                displayUsage();
                exit(0); 
        }
    }

    
    // Spit out all the parameters in case things are wrong
    cout << "In path: '" << in_path << "' Out path: '" << out_path << "'\n";
    cout << "Mode " << mode << " Expand " << expand << " Width " << width << " Height " << height << endl;

    // Parameters check
    if (in_path == "") { cout << "Input path empty" << endl; return 1; }
    if (out_path == "") { cout << "Output path empty" << endl; return 2; }
    //if (mode == "") { cout << "Mode empty" << endl; return 2; }

    vector <string> files = arrayFilesName(in_path);
    sort (files.begin(), files.end());
    cout << "Files found:" << endl;
    for (int i=0; i < (int)files.size(); i++){
        cout << "'" << files[i] << "'" << endl;
    }
    cout << endl;
    cout << "Output files stored on: '" << out_path << "' directory\n" << endl;

    struct stat st = {0};
    if (stat(out_path.c_str(), &st) == -1) { // if output directory does not exist, create it
        mkdir(out_path.c_str(), 0700);
    }
    
   
    for (int i=0; i < (int)files.size(); i++) {
        string out_file;    // ***SAV previous version used to overwrite "out_path"
        file_name = files[i].substr(0,files[i].find_last_of ("."));
        out_file = out_path + "/" + file_name;
        if (mode==4)
            out_file+= string(CSV_SUFFIX);
        else
            out_file += string(XML_SUFFIX);
        string in_file = in_path+"/"+files[i];
        // we need to ignore if it is not a file
        cout << "Basename '" << file_name << "' Infile: '"<< in_file<<"' Outfile '"<< out_file << "'\n";
        if (!is_file(in_file.c_str())) {
            cout << "'" << in_file << "' is not a normal file, ignored\n";
            continue; // ignore, something went wrong
        }
        cout << "CASE " << mode << endl;
        switch(mode){
            case 1:
                viper2dam(in_file, out_file);
                break;
            case 2:
                mat2damDet(in_path+"/"+files[i], out_file);
                break;

            case 3:
                mat2damTrack(in_path+"/"+files[i], out_file);
                break;
            case 4:
                viper2csv (in_file, out_file, expand, width, height);
                break;
            case 5:
                viper2yolo (file_name, in_file, out_path, expand, width, height); // for YOLO note: out_path
                break;
        }
        
    }
    
    cout << "FILTER FINISHED!" << endl << endl;
    return 0;
}
