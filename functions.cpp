//S.A. Velastin 2016, UC3M
//Adapted by M. González 2017
#include "functions.hpp"
#include <sys/stat.h>

#include <iomanip>

//#define DEBUG

//#define ENABLEPAUSE
static void pause(string msg)
{
    std::string dummy;
    std::cout << msg << "... ";
#ifdef ENABLEPAUSE
    std::getline(std::cin, dummy);
#else
        std::cout << endl;
#endif
}

//***SAV to check if path is a normal file (http://forum.codecall.net/topic/68935-how-to-test-if-file-or-directory/)
bool is_file(const char* path) {
    struct stat buf;
    stat(path, &buf);
    return S_ISREG(buf.st_mode);
}


istream& operator>>(istream& str, CSVRow& data){
    data.readNextRow(str);
    return str;
}

vector <string> arrayFilesName(string folderPath){
    
    vector<string> files;

    DIR *pDIR;
    struct dirent *enter;
    if( pDIR = opendir(folderPath.c_str()) ){
        while(enter = readdir(pDIR)){
                string fileName = enter->d_name;
                if( fileName != "." && fileName != ".." ){
                    files.push_back(fileName);
                }
        }
        closedir(pDIR);
    }

    return files;
}

bool compareByframe(const Head &head1, const Head &head2){
    return head1.frame < head2.frame;
}

// ***SAV modified so that it returns -1 on an error
// TODO: note that this is very specific to the annotation schema e.g. it uses "Cabeza"
// reads the XML and puts result in "heads"
static int readXMLviper2DamGT(string in_path, vector<Head>& heads, int &NFRAMES){
    
    NFRAMES=-1;     // default in case it is not found
    TiXmlDocument doc;
	if(!doc.LoadFile(in_path.c_str())){
		cerr << "File couldn't be opened: '" << in_path << "'" << endl;
		return -1;
	}
	TiXmlElement* root = doc.FirstChildElement();
    string id;

	for(TiXmlElement* elem = root->FirstChildElement(); elem != NULL; elem = elem->NextSiblingElement()){
    	string elemName = elem->Value();
    	if(elemName == "data"){
    		for(TiXmlElement* elem2 = elem->FirstChildElement(); elem2 != NULL; elem2 = elem2->NextSiblingElement()){
    			elemName = elem2->Value(); 				
    			if(elemName == "sourcefile"){
    				for(TiXmlElement* elem3 = elem2->FirstChildElement(); elem3 != NULL; elem3 = elem3->NextSiblingElement()){
    					elemName = elem3->Value();
                        cout << "Elem " << elemName << endl;
                        if (elemName == "file") {
                            for(TiXmlElement* elem4 = elem3->FirstChildElement(); elem4 != NULL; elem4 = elem4->NextSiblingElement()){
                                cout <<"   Elemname '" << elem4->Attribute("name") << "'" << endl;
                                if ((elemName =elem4->Attribute("name")) == "NUMFRAMES") {
    								for(TiXmlElement* elem5 = elem4->FirstChildElement(); elem5 != NULL; elem5 = elem5->NextSiblingElement()){
                                        char* frame = const_cast<char*>(elem5->Attribute("value"));
                                        NFRAMES=atoi(frame);
                                        cout << "Number of Frames: " << NFRAMES <<endl;
                                    }
                                    cout << endl;
                                }
                            }
                        }
    					if(elemName == "object"){
                            id = elem3->Attribute("id");
                            for(TiXmlElement* elem4 = elem3->FirstChildElement(); elem4 != NULL; elem4 = elem4->NextSiblingElement()){
                                if((elemName = elem4->Attribute("name"))=="Cabeza"){
				  pause("Cabeza found");
    								for(TiXmlElement* elem5 = elem4->FirstChildElement(); elem5 != NULL; elem5 = elem5->NextSiblingElement()){
    									Head head;
    									stringstream s;
									int frame_start, frame_end;
    									char* frame = const_cast<char*>(elem5->Attribute("framespan"));
									cout << "Framespan: '" << frame << "'\n";
    									frame_start = atoi(strtok(frame, ":"));
									frame_end = atoi (strtok(NULL,":"));
									cout << frame_start << "-" << frame_end << "\n";
									// Now generate heads for the frame span
									for (int i = frame_start; i <= frame_end; i++) {
									  head.frame = i;
									  s << id << " " << elem5->Attribute("x") << " " << elem5->Attribute("y") << " " << elem5->Attribute("width") << " " << elem5->Attribute("height") << " " << elem5->Attribute("rotation");
									  s >> head.id >> head.x >> head.y >> head.w >> head.h >> head.rotation;
									  cout << head.frame << " " << head.id << " " << head.x << " " << head.y << " " << head.w << " " << head.h << "\n";
									  heads.push_back(head);
									}
									pause("that was the frame span");
    								}
                                }
    						}
    					}
    				}
    			}
    		}
    	}
    }
}

void writeXMLviper2DamGT(string out_path, vector<Head>& heads){
    
    int frame = -1;
    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "utf-8", "no");
    doc.LinkEndChild(decl);
    
    TiXmlElement* root = new TiXmlElement("viperGT2damienGT");
    doc.LinkEndChild(root);
    TiXmlElement* element1;

    for(unsigned i = 0; i < heads.size(); i++){

        // Escribir el XML con la información del vector
        if(frame != heads[i].frame){
            element1 = new TiXmlElement("frame");
            root->LinkEndChild(element1);
            element1->SetAttribute("id", heads[i].frame);
            frame = heads[i].frame;

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("id", heads[i].id);

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("x", heads[i].x);
            element3->SetAttribute("y", heads[i].y);
            element3->SetAttribute("width", heads[i].w);
            element3->SetAttribute("height", heads[i].h);
            
        }else{
            frame = heads[i].frame;

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("id", heads[i].id);

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("x", heads[i].x);
            element3->SetAttribute("y", heads[i].y);
            element3->SetAttribute("width", heads[i].w);
            element3->SetAttribute("height", heads[i].h);
        }
    }

    // Guardar fichero XML
    doc.SaveFile(out_path);
    // Liberar variable
    doc.Clear();

}

// ***SAV detects if read ... had a problem
void viper2dam(string in_path, string out_path){
    vector<Head> heads;
    int NFRAMES;

    cout << "*** VIPER2DAM In: '" << in_path << "' Out: '" << out_path << "'\n";
    if (readXMLviper2DamGT(in_path, heads, NFRAMES) == -1) return;    
    
    sort(heads.begin(), heads.end(), compareByframe); // sorts out by frame number

    if(heads.empty()){
        cout << "No people have been found in the video." << endl; // when the whole file has no "heads"

    }else{
        cout << "Found " << heads.size() << " heads in the file." << endl << endl;
        // Now we write
        writeXMLviper2DamGT(out_path, heads);
    }
    
}


//==============================================================
// ***SAV 2018   This has been added to extract ground truth e.g. for YOLO etc.
// This function looks at InHead and the conversion parameters to work out a "new" NewHead, returning false on error
// The code might seem more complicated than necessary but that it serves YOLO and CSV
// We assume Head (x,y,w,h), where x,y are the TOP LEFT box coordinates
// We also assume that (x,y,w,h) result in a bounding box that is within 0..maxwidth-1, 0..maxheight-1
static bool process_head (const Head InHead, Head &NewHead, int expand, int maxwidth, int maxheight, bool correct=true) {
        ostringstream ErrorSt; // we maintain a message here so output is cleaner
        float factor=expand/100.0;  // easier to work with a fraction
        NewHead=InHead; // we copy first, so if we return we return the same values
        
        // expand if required
        if (expand!=0) {
            // In this version we will make sure that we keep the expanded version centered in the same position as the original
            int centre_x, centre_y;     // the centroid
            centre_x=NewHead.x+NewHead.w/2;   // We assume the ground truth is correct and that this cannot go out of bounds
            centre_y=NewHead.y+NewHead.h/2;
                       
            NewHead.w = NewHead.w*(1+factor)+0.5;   // expand the width and height
            NewHead.h = NewHead.h*(1+factor)+0.5;
//#ifdef DEBUG            
            cout << "CX: " << centre_x << " CY: " << centre_y << " W': " << NewHead.w << " H'" << NewHead.h << endl;
//#endif            
            // The new TL corner will have coordinates CX-W/2, CY-H/2
            NewHead.x = centre_x - NewHead.w/2;     // these might go negative!
            NewHead.y = centre_y - NewHead.h/2;
            int maxx, maxy;         // bottom left coordinates
            maxx=NewHead.x+NewHead.w;       // these might go beyond the image limits
            maxy=NewHead.y+NewHead.h;
            // Now we check if everything is ok
            if ((NewHead.x >=0)&&(maxx<maxwidth)&&(NewHead.y>=0)&&(maxy<maxheight)) {
//#ifdef DEBUG
                // output what is in Inhead
                cout << "(" <<InHead.frame << "," << InHead.id << ") " << InHead.x<<" "<<InHead.y<<" "<<InHead.w<<" "<<InHead.h<<"-->";
                cout << "(" << NewHead.frame << "," << NewHead.id << ") " << NewHead.x<<" "<<NewHead.y<<" "<<NewHead.w<<" "<<NewHead.h<<"\n";
		pause("check results please");
                return true;        // all is well
            }
//#endif        
            // The bounding box would go out of bounds so we need correction
            cout << "We have a problem! ";
            cout << "(" <<InHead.frame << "," << InHead.id << ") " << InHead.x<<" "<<InHead.y<<" "<<InHead.w<<" "<<InHead.h<<"-->";
            cout << NewHead.x<<" "<<NewHead.y<<" "<<NewHead.w<<" "<<NewHead.h;
            cout << "(CX:" << centre_x << ", CY:" << centre_y <<") (" << maxx << "," << maxy <<")\n";
            if (!correct)
                return false;       // we have been asked not to correct problems, so we just return false
            
            //pause("!");
            // we deal with the errors. Sounds long-winded but we treat each case independently
            if (NewHead.x < 0) { // we went out of left bounds
                NewHead.w = 2*centre_x;     // this is the maximum with we can have
                // So a corrected expansion factor is:
                expand = 100*((float)NewHead.w/InHead.w-1);
                cout << "top x out of bounds, New expansion factor is: " << expand << endl;
                //pause("  topx...");
                // And we try again (recursion! this ensures that height is also expanded by same factor etc.)
                return process_head (InHead, NewHead, expand, maxwidth, maxheight, correct);
            }    
            if (NewHead.y < 0) { // we went out of top bounds
                NewHead.h = 2*centre_y;     // this is the maximum with we can have
                // So a corrected expansion factor is:
                expand = 100*((float)NewHead.h/InHead.h-1);
                cout << "top y out of bounds, New expansion factor is: " << expand;
                pause("  topy ...");
                // And we try again (recursion! this ensures that height is also expanded by same factor etc.)
                return process_head (InHead, NewHead, expand, maxwidth, maxheight, correct);
            }    
            if (maxx >= maxwidth) { // we went out of left bounds
                NewHead.w = 2*(maxwidth-1-centre_x);     // this is the maximum with we can have
                // So a corrected expansion factor is:
                expand = 100*((float)NewHead.w/InHead.w-1);
                cout << "bottom x out of bounds, New expansion factor is: " << expand;
                //pause("  botx...");
                // And we try again (recursion! this ensures that height is also expanded by same factor etc.)
                return process_head (InHead, NewHead, expand, maxwidth, maxheight, correct);
            }    
            if (maxy >= maxheight) { // we went out of left bounds
                NewHead.h = 2*(maxheight-1-centre_y);     // this is the maximum with we can have
                // So a corrected expansion factor is:
                expand = 100*((float)NewHead.h/InHead.h-1);
                cout << "bottom y out of bounds, New expansion factor is: " << expand;
                //pause("  boty...");
                // And we try again (recursion! this ensures that height is also expanded by same factor etc.)
                return process_head (InHead, NewHead, expand, maxwidth, maxheight, correct);
            }    
            pause("We should not have reached here! no error condition detected");
            return false;
        } // expand != 0

        return true;
}

// Output to a CSV file
// We assume heads has been sorted by frame no.
// We take the approach that any "funnies" indicate that it is better to ignore a sample
static void writeXMLviper2csvGT(string out_path, vector<Head>& heads, int expand, int maxwidth, int maxheight){
    
    ofstream outfile;   // we were told where to write
    outfile.open (out_path.c_str(), ios::out);
    
    for(unsigned i = 0; i < heads.size(); i++){
        Head Bbox;
        if (!process_head (heads[i], Bbox, expand, maxwidth, maxheight)) 
            continue;   // something did not work
            
        // here we simply spit out the data in "heads"
        outfile << heads[i].frame << ",";
        outfile << heads[i].id << ",";
        outfile << "head" << ",";
        outfile << Bbox.x <<"," << Bbox.y << "," << Bbox.w << "," << Bbox.h << endl;
    }
    outfile.close();
}

void viper2csv(string in_path, string out_path, int expand, int maxwidth, int maxheight){
    vector<Head> heads;
    int NFRAMES;

    cout << "*** VIPER2CSV In: '" << in_path << "' Out: '" << out_path << "'\n";
    if (readXMLviper2DamGT(in_path, heads, NFRAMES) == -1) return;    
    
    sort(heads.begin(), heads.end(), compareByframe); //***SAV this ensures it is ordered by frame

    if(heads.empty()){
        cout << "No people have been found in the video." << endl; // when no people in the whole file

    }else{
        cout << "Found " << heads.size() << " heads in the file." << endl << endl;
        // Now we write
        writeXMLviper2csvGT(out_path, heads, expand, maxwidth, maxheight);
    }
}

// We assume that the image files were created with this suffix after the basename
#define IMG_PATTERN "_%06d"
#define TXTWIDTH 6

// Generate files for YOLO training
// We assume heads has been sorted by frame no.
// We take the approach that any "funnies" indicate that it is better to ignore a sample
// This is similar to writeXMLviper2yoloGT except that for each frame we output one file
// We also generate empty files for empty frames
static void writeXMLviper2yoloGT(string basename, string out_path, vector<Head>& heads, int expand, int maxwidth, int maxheight, int NFRAMES){

    int prev_frame = 0; // this indicates that we have not seen any data yet
    ofstream outfile;
   
    for (unsigned i = 0; i < heads.size(); i++) {
        Head Bbox;
        if (!process_head (heads[i], Bbox, expand, maxwidth, maxheight)) 
            continue;   // something did not work
        // Here we process the data, lets see if we have a new frame

//#ifdef DEBUG        
        cout << "Frame: " << Bbox.frame << " prev_frame " << prev_frame << endl;
//#endif
        
        if (Bbox.frame!=prev_frame) {
            // If we have jumped by more than 1 frame we create empry files (needed by YOLO)
            for (int frame=prev_frame+1;  frame<Bbox.frame; frame++) {
                // Here we create a filename with leading zeros
                ostringstream filename;
                filename << basename  << "_" << setfill('0') << setw(TXTWIDTH) << frame << ".txt";
                string fname=out_path + "/" + filename.str();

//#ifdef DEBUG
                cout << "New Frame!, EMPTY file '" << filename.str() << "'" << " Full path '" << fname << "'" << endl;
//#endif
            
                // We need to close the current file if open
                if (outfile.is_open()) outfile.close();
                outfile.open (fname.c_str(), ios::out);     // now create and open a new file
                if (!outfile.is_open()) {
                    cerr << "***Warning, file '" << fname << "' failed to open\n";
                    exit(4);
                }
            } // for (empty files)

            // Here we create a filename (Bbox.frame) with leading zeros (this will have data)
            ostringstream filename;
            filename << basename  << "_" << setfill('0') << setw(TXTWIDTH) << Bbox.frame << ".txt";
            string fname=out_path + "/" + filename.str();

//#ifdef DEBUG
            cout << "New Frame!, file '" << filename.str() << "'" << " Full path '" << fname << "'" << endl;
//#endif
            
            // We need to close the current file if open
            if (outfile.is_open()) outfile.close();
            outfile.open (fname.c_str(), ios::out);     // now create and open a new file
            if (!outfile.is_open()) {
                cerr << "***Warning, file '" << fname << "' failed to open\n";
                exit(4);
            }
            prev_frame = Bbox.frame;    // remember it
        } // different to previous frame
        if (outfile.is_open()) {  // output class (x1+x2)/2 (y1+y2)/2 w h (relative)
            // We do it explicitly for clarity
            float xmin=Bbox.x, xmax=Bbox.x+Bbox.w-1, ymin=Bbox.y, ymax=Bbox.y+Bbox.h-1;
            float x1=(xmax+xmin)/2, y1=(ymax+ymin)/2, w=Bbox.w, h=Bbox.h;

#ifdef DEBUG            
            cout << Bbox.frame << " (" << Bbox.x <<","<<Bbox.y<<","<<Bbox.w<<","<<Bbox.h<<")\n";
            cout << Bbox.frame << " (" << x1 <<","<<y1<<","<<w<<","<<h<<")\n";
#endif            
            
            // Normalise what needs to be output
            x1 /= maxwidth;
            y1 /= maxheight;
            w /= maxwidth;
            h /= maxheight;
            
            
#ifdef DEBUG            
            cout << Bbox.frame << " (" << x1 <<","<<y1<<","<<w<<","<<h<<")\n";
            pause("how about that?");
#endif            

            outfile << "0 "<< x1 <<" "<<y1<<" "<<w<<" "<<h<<"\n";
        } // generate the output
    } // for all the heads
    // At this point, prev_frame should be the last frame (file) we generated
    if (outfile.is_open()) outfile.close();  // this will be for the last entry
    // We now generate a remainder of empty files, if applicable
            for (int frame=prev_frame+1;  frame<= NFRAMES; frame++) {
                // Here we create a filename with leading zeros
                ostringstream filename;
                filename << basename  << "_" << setfill('0') << setw(TXTWIDTH) << frame << ".txt";
                string fname=out_path + "/" + filename.str();

//#ifdef DEBUG
                cout << "New Frame!, EMPTY file '" << filename.str() << "'" << " Full path '" << fname << "'" << endl;
//#endif
            
                // We need to close the current file if open
                outfile.open (fname.c_str(), ios::out);     // now create and open a new file
                if (!outfile.is_open()) {
                    cerr << "***Warning, file '" << fname << "' failed to open\n";
                    exit(4);
                }
                outfile.close(); // and close the file
            } // for (empty files)
}

// This is the public function to generate YOLO ground truth files
void  viper2yolo (const string basename, const string in_path, const string out_path, int expand, int maxwidth, int maxheight) {
    vector<Head> heads;
    int NFRAMES;

    cout << "*** VIPER2YOLO basename: '"<<basename<<"' In: '" << in_path << "' Out path: '" << out_path << "'\n";
    if (readXMLviper2DamGT(in_path, heads, NFRAMES) == -1) return;    
    
    sort(heads.begin(), heads.end(), compareByframe); //***SAV this ensures it is ordered by frame

    if(heads.empty()){
        cout << "No people have been found in the video." << endl; // when no people in the whole file

    }else{
        cout << "Found " << heads.size() << " heads in the file." << endl << endl;
        // Now we write
        writeXMLviper2yoloGT(basename, out_path, heads, expand, maxwidth, maxheight, NFRAMES);
    }
}
//=====================================================================

void mat2damDet(string in_path, string out_path){
    ifstream       file(in_path.c_str(), ios::in | ios::binary);
    CSVRow         row;

    vector<string> rowCont;
    string frame = "";

    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "utf-8", "no");
    doc.LinkEndChild(decl);
    
    TiXmlElement* root = new TiXmlElement("Detection_Mathieu");
    doc.LinkEndChild(root);
    TiXmlElement* element1;

    while(file >> row){

        // Recorrer fila de CSV y almacenar contenido en vector
        for (unsigned count = 0; count < row.size() ; count++){
            rowCont.push_back(row[count]);
        }

        // Escribir el XML con la información del vector
        if(frame.compare(rowCont[0]) != 0){
            element1 = new TiXmlElement("frame");
            root->LinkEndChild(element1);
            element1->SetAttribute("id", rowCont[0]);
            frame = rowCont[0];

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("confidence", "XXX");
            element2->SetAttribute("id", rowCont[1]);
            element2->SetAttribute("idGroup", "XXX");
            element2->SetAttribute("rotation", "XXX");

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("height", rowCont[5]);
            element3->SetAttribute("width", rowCont[4]);
            element3->SetAttribute("x", rowCont[2]);
            element3->SetAttribute("y", rowCont[3]);

        }else{
            frame = rowCont[0];

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("confidence", "XXX");
            element2->SetAttribute("id", rowCont[1]);
            element2->SetAttribute("idGroup", "XXX");
            element2->SetAttribute("rotation", "XXX");

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("height", rowCont[5]);
            element3->SetAttribute("width", rowCont[4]);
            element3->SetAttribute("x", rowCont[2]);
            element3->SetAttribute("y", rowCont[3]);
        }
        // Vaciar vector
        rowCont.clear();
    }

    // Guardar fichero XML
    doc.SaveFile(out_path);
    
    // Liberar variable
    doc.Clear();
}

void mat2damTrack(string in_path, string out_path){
    ifstream       file(in_path.c_str(), ios::in | ios::binary);
    CSVRow         row;
    int width = 56, height = 56;     //MGM: asuming it is constant

    vector<string> rowCont;
    string frame = "";

    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "utf-8", "no");
    doc.LinkEndChild(decl);
    
    TiXmlElement* root = new TiXmlElement("Tracking_Mathieu");
    doc.LinkEndChild(root);
    TiXmlElement* element1;

    while(file >> row){

        // Recorrer fila de CSV y almacenar contenido en vector
        for (unsigned count = 0; count < row.size() ; count++){
            rowCont.push_back(row[count]);
            //cout << "Type of " << row[count] << ": " << typeid(row[count]).name() << endl;
        }
        
        int x_i, y_i, tmp_i;
        string x_s, y_s;
        ostringstream tmp_s;

        stringstream(rowCont[2]) >> tmp_i;
        x_i = tmp_i - (width/2);
        tmp_i = 0;
        stringstream(rowCont[3]) >> tmp_i;
        y_i = tmp_i - (height/2);

        tmp_s << x_i;
        x_s = tmp_s.str();
        tmp_s.str("");
        tmp_s << y_i;
        y_s = tmp_s.str();

        // Escribir el XML con la información del vector
        if(frame.compare(rowCont[0]) != 0){
            element1 = new TiXmlElement("frame");
            root->LinkEndChild(element1);
            element1->SetAttribute("id", rowCont[1]);
            frame = rowCont[0];

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("confidence", "XXX");
            element2->SetAttribute("id", rowCont[0]);
            element2->SetAttribute("idGroup", "XXX");
            element2->SetAttribute("rotation", "XXX");

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("height", height);
            element3->SetAttribute("width", width);
            element3->SetAttribute("x", x_s);
            element3->SetAttribute("y", y_s);

        }else{
            frame = rowCont[0];

            TiXmlElement* element2 = new TiXmlElement("object");
            element1->LinkEndChild(element2);
            element2->SetAttribute("confidence", "XXX");
            element2->SetAttribute("id", rowCont[0]);
            element2->SetAttribute("idGroup", "XXX");
            element2->SetAttribute("rotation", "XXX");

            TiXmlElement* element3 = new TiXmlElement("rectangle");
            element2->LinkEndChild(element3);
            element3->SetAttribute("height", height);
            element3->SetAttribute("width", width);
            element3->SetAttribute("x", x_s);
            element3->SetAttribute("y", y_s);
        }
        // Vaciar vector
        rowCont.clear();
    }

    // Guardar fichero XML
    doc.SaveFile(out_path);
    
    // Liberar variable
    doc.Clear();
}

